# raspbian-tiny

Further reduce the number of packages in a Raspianan lite installation.
The scripts are compatible with Raspbian Buster (as of December 2019).

The scripts are based on https://gist.github.com/hhromic/78e3d849ec239b6a4789ae8842701838.
# install only man-pages for documentation
cat > /etc/dpkg/dpkg.cfg.d/nodoc << EOF
path-exclude /usr/share/doc/*
path-include /usr/share/doc/*/copyright
path-exclude /usr/share/groff/*
path-exclude /usr/share/info/*
path-exclude /usr/share/lintian/*
path-exclude /usr/share/linda/*
EOF

# remove already present documentation
find /usr/share/doc -depth -type f ! -name copyright | xargs rm
find /usr/share/doc -empty | xargs rmdir
rm -rf /usr/share/lintian/* /usr/share/linda/*

# do not add locales
cat > /etc/dpkg/dpkg.cfg.d/nolocale << EOF
path-exclude /usr/share/locale/*
EOF

rm -rf /usr/share/locale/*

cat > /etc/dpkg/dpkg.cfg.d/noman << EOF
path-exclude /usr/share/man/*
path-include /usr/share/man/man*/*
EOF

find /usr/share/man/ -depth -type d ! -name man ! -name man\? | xargs rm -rf

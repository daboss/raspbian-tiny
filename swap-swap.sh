
[ -f /var/swap ] && swapoff /var/swap
rm -f /var/swap

xargs apt -y purge << EOF
dphys-swapfile
EOF

apt autoremove -y --purge

xargs apt -y install << EOF
zram-tools
EOF


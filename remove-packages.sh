
# purge non-critical packages
xargs apt -y purge << EOF
aptitude 
aptitude-common 
apt-listchanges 
apt-utils 
blends-tasks 
build-essential
bzip2 
cifs-utils 
console-setup 
console-setup-linux 
cpp 
debconf-i18n 
dmidecode 
dosfstools
dpkg-dev 
ed 
gcc 
gcc-4.9-base 
gcc-5-base 
gcc-6 gdb
geoip-database 
gettext-base 
hardlink 
htop 
info 
install-info 
iptables 
iputils-ping
isc-dhcp-client 
isc-dhcp-common 
kbd 
keyboard-configuration 
libglib2.0-data
liblocale-gettext-perl 
libtext-charwidth-perl 
libtext-iconv-perl 
libtext-wrapi18n-perl 
luajit 
make 
manpages-dev 
mime-support 
ncdu 
netcat-openbsd
netcat-traditional 
net-tools 
nfs-common 
ntfs-3g
perl 
plymouth 
python 
rpcbind 
rsyslog 
samba-common
sgml-base 
shared-mime-info 
strace 
tasksel 
tasksel-data 
tcpd 
traceroute 
triggerhappy 
usb-modeswitch 
usb-modeswitch-data 
usbutils 
v4l-utils 
xauth
xdg-user-dirs 
xxd 
xz-utils 
zlib1g-dev
EOF

# autoremove
apt -y autoremove --purge


# mark all libraries as automatically installed
dpkg-query -Wf '${binary:Package}\n' 'lib*[!raspberrypi-bin]' | xargs apt-mark auto

